const fs = require('fs');

function modelExists(comp) {
  try {
    fs.accessSync(`server/routes/${comp}.js`, fs.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

module.exports = {
  description: 'Add server routes',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'user',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return modelExists(value) ? 'A route with this name already exists' : true;
      }
      return 'The name is required';
    },
  }],
  actions: (data) => {
    const actions = [{
      type: 'add',
      path: '../../server/routes/{{camelCase name}}.js',
      templateFile: './route/route.js.hbs',
      abortOnFail: true,
    }];
    return actions;
  },
};
