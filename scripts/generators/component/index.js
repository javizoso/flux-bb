/**
 * Component Generator
 */

const fs = require('fs');
const components = fs.readdirSync('client/src/components');

function componentExists(comp) {
  return components.indexOf(comp) >= 0;
}

module.exports = {
  description: 'Add an unconnected component',
  prompts: [{
    type: 'list',
    name: 'type',
    message: 'Select the type of component',
    default: 'Stateless Function',
    choices: () => ['Stateless Function', 'ES6 Class (Pure)', 'ES6 Class'],
  }, {
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'Button',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return componentExists(value) ? 'A component or container with this name already exists' : true;
      }
      return 'The name is required';
    },
  }, {
    type: 'confirm',
    name: 'wantCSS',
    default: true,
    message: 'Do you want css?',
  },{
    type: 'confirm',
    name: 'wantTests',
    default: true,
    message: 'Do you want tests?',
  }],
  actions: (data) => {
    let componentTemplate;
    if (data.type === 'ES6 Class')
      componentTemplate = './component/es6.js.hbs';
    else if (data.type === 'ES6 Class (Pure)')
      componentTemplate = './component/es6.pure.js.hbs';
    else if (data.type === 'Stateless Function')
      componentTemplate = './component/stateless.js.hbs';
    else
      componentTemplate = './component/es6.js.hbs';

    const actions = [{
      type: 'add',
      path: '../../client/src/components/{{properCase name}}/{{ properCase name }}.js',
      templateFile: componentTemplate,
      abortOnFail: true,
    },{
      type: 'add',
      path: '../../client/src/components/{{properCase name}}/package.json',
      templateFile: './component/package.json.hbs',
      abortOnFail: true,
    }];

    if (data.wantCSS) {
      actions.push({
        type: 'add',
        path: '../../client/src/components/{{properCase name}}/{{ properCase name }}.scss',
        templateFile: './component/styles.scss.hbs',
        abortOnFail: true,
      });
    }

    if (data.wantTests) {
      actions.push({
        type: 'add',
        path: '../../client/src/components/{{properCase name}}/{{ properCase name }}.test.js',
        templateFile: './component/test.js.hbs',
        abortOnFail: true,
      });
    }

    return actions;
  },
};
