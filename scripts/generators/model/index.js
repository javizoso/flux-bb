const fs = require('fs');

function itExists(path, comp) {
  try {
    fs.accessSync(`${path}/${comp}.js`, fs.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

module.exports = {
  description: 'Add a server model',
  prompts: [{
    type: 'input',
    name: 'name',
    message: 'What should it be called?',
    default: 'user',
    validate: (value) => {
      if ((/.+/).test(value)) {
        return itExists('server/models', value) ? 'A model with this name already exists' : true;
      }
      return 'The name is required';
    },
  },{
    type: 'confirm',
    name: 'wantRoutes',
    default: true,
    message: 'Do you want to create routes as well?',
  }],
  actions: (data) => {
    const actions = [{
      type: 'add',
      path: '../../server/models/{{camelCase name}}.js',
      templateFile: './model/model.js.hbs',
      abortOnFail: true,
    }];
    if (data.wantRoutes) {
      actions.push({
        type: 'add',
        path: '../../server/routes/{{camelCase name}}.js',
        templateFile: './route/route.js.hbs',
        abortOnFail: true,
      });
    }
    return actions;
  },
};
