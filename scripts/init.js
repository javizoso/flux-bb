require('shelljs/global');

let dir = pwd().split('/')
let dashed = dir[dir.length-1]
let title = dashed.split('-').map((a) => a.charAt(0).toUpperCase() + a.slice(1)).join(' ')

exec('find . -path ./node_modules -prune -o -name "*.*" -type f -print', (code, stdout, stderr) => {
  let files = stdout.trim().split("\n")
  files.map((file) => {
    sed('-i', /flux-kit/, dashed, file);
    sed('-i', /Flux Kit/, title, file);
  })
})
