const Path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = (options) => {
  const ExtractSASS = new ExtractTextPlugin(`/styles/${options.cssFileName}`);

  const webpackConfig = {
    devtool: options.devtool,
    entry: [
      `webpack-dev-server/client?http://localhost:${+ options.port}`,
      Path.join(__dirname, '../src/index'),
    ],
    output: {
      path: Path.join(__dirname, '../dist'),
      filename: `scripts/${options.jsFileName}`,
    },
    resolve: {
      extensions: ['', '.js', '.jsx'],
    },
    module: {
      loaders: [{
        test: /.jsx?$/,
        include: Path.join(__dirname, '../src'),
        // include: [Path.join(__dirname, '../src'), Path.join(__dirname, '../../../flux-ui')],
        // exclude: /node_modules/,
        loader: 'babel',
        cacheDirectory: true,
        query: {
          plugins: [
            'transform-class-properties',
            'transform-decorators-legacy',
            'transform-function-bind',
            'transform-object-rest-spread',
            ['import', [{ libraryName: "antd", style: 'css' }]],
          ],
        },
      }, {
        test: /\.woff2$/, 
        loader: 'url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]'
      }, { 
        test: /\.(png)$/,
        loader: 'url-loader?name=images/[name].[ext]&limit=8192' 
      }]
    },
    plugins: [
      new Webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development'),
        },
      }),
      new HtmlWebpackPlugin({
        template: Path.join(__dirname, '../static/index.html'),
      }),
    ],
  };

  if (options.isProduction) {
    webpackConfig.entry = [Path.join(__dirname, '../src/index')];

    webpackConfig.plugins.push(
      new Webpack.optimize.OccurenceOrderPlugin(),
      // new Webpack.optimize.UglifyJsPlugin({
      //   compressor: {
      //     warnings: false,
      //   },
      // }),
      ExtractSASS
    );

    webpackConfig.module.loaders.push({
      test: /\.scss$/,
      loader: ExtractSASS.extract(['css', 'sass']),
      // loaders: ExtractSASS.extract(['style', 'css', 'sass']),
    });
    webpackConfig.module.loaders.push({
      test: /\.css$/,
      loaders: ['style', 'css'],
    });
  } else {
    
    webpackConfig.module.loaders.push({
      test: /\.scss$/,
      loaders: ['style', 'css', 'sass'],
    });

    webpackConfig.module.loaders.push({
      test: /\.css$/,
      loaders: ['style', 'css'],
    });

    webpackConfig.devServer = {
      contentBase: Path.join(__dirname, '../'),
      port: options.port,
      inline: true,
      progress: true,
      historyApiFallback: true,
      proxy: {
        '/ws': { target: 'ws://localhost:3001', ws: true },
        '/images': { target: 'http://localhost:3001', secure: false },
        '/api': { target: 'http://localhost:3001', secure: false },
        '/login': { target: 'http://localhost:3001', secure: false },
      },
    };
  }

  return webpackConfig;
};
