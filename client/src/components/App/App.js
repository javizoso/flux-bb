import React from 'react';
import { Route } from 'react-router-dom';
// import Layout from '../../../../../flux-ui/lib/components/layout';
import { Layout } from 'antd';
import MapComponent from '../Map'
import './App.scss';
const { Header, Footer, Sider, Content } = Layout;
import ListHazards from '../ListHazards';
import data from '../../lib/data';
import ChartPie from '../ChartPie';
import ChartBar from '../ChartBar';
import { observer, inject } from 'mobx-react'
import filterIcon from '../../../static/filter2.png'
import dateIcon from '../../../static/date.png'
import { Radio, Input, Checkbox, Popover, Button } from 'antd';
const RadioGroup = Radio.Group;



@inject('hazard')
@observer
class App extends React.Component {

  constructor(props) {
    super(props)
    // this.state = {filter: false}
    this.state = {filter: false, date: false, dateValue: 0, red: true, amber: true, green: false, health: true, excavations: true, catastrophe: true, pv: true, height: true, electricity: true, lifting: true, buried: true, traffic: true}
  }

  onFilter() {
    this.setState({filter: !this.state.filter})
  }

  onDate() {
    this.setState({date: !this.state.date})
  }

  onSetDate(e) {
    this.setState({dateValue: e.target.value})
    this.props.hazard.setDate(e.target.value)
  }

  render() {
    let data = this.props.hazard.visible
    let staticdata = this.props.hazard.hazards
    let filters = this.props.hazard.filters
    // console.log('render app', this.state)
    if (!data || !data.length) return null
    // data = data.list
  // this.props.hazard.filter.bind(this.props.hazard)
      // <a onClick={this.hide}>Close</a>
    let onChange = function(type, e) {
      // let state = {}
      // state[type] = e.target.checked
      // this.setState(state)
      this.props.hazard.setFilter(type, e)
    }
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };
    let dateContent = (
      <div className='date-menu'>
        <RadioGroup onChange={this.onSetDate.bind(this)} value={this.state.dateValue}>
          <Radio style={radioStyle} value={0}>This week</Radio>
          <Radio style={radioStyle} value={1}>All time</Radio>
        </RadioGroup>
      </div>
    )
    let filterContent = (
      <div className='filter-menu'>
        <div className='label'>Severity</div>
        <Checkbox disabled checked onChange={onChange.bind(this, 'red')}>Red</Checkbox>
        <Checkbox disabled checked onChange={onChange.bind(this, 'amber')}>Amber</Checkbox>
        <Checkbox defaultChecked={filters.green} onChange={onChange.bind(this, 'green')}>Green</Checkbox>
        <div className='label second'>Category</div>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'health')}>Health</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'excavations')}>Excavations</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'catastrophe')}>Catastrophe</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'pv')}>People Vehicle Plant</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'height')}>Working at Height</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'electricity')}>Electricity</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'lifiting')}>Lifting</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'buried')}>Buried Services</Checkbox>
        <Checkbox defaultChecked onChange={onChange.bind(this, 'traffic')}>Traffic</Checkbox>
      </div>
    )
    let filterClasses = 'filter'
    if (this.state.filter) filterClasses += ' open'
    let dateClasses = 'date'
    if (this.state.date) dateClasses += ' open'
             // <div className='filter' onClick={() => console.log('foo')}>
             //   <div className='label'>filter</div>
             //   <img src={icon}/>
             // </div>
    return (
      <div className="container">
        <div className='container-left'>
          <div className='header'>
            <div className='app-title'>AppHazard</div>


            <Popover
              content={dateContent}
              // className='filter'
              // title="Title"
              trigger="click"
              placement='bottomRight'
              visible={this.state.date}
              onVisibleChange={this.onDate.bind(this)}
            >
              <Button className={dateClasses}><img src={dateIcon}/></Button>
            </Popover> 
            <Popover
              content={filterContent}
              // className='filter'
              // title="Title"
              trigger="click"
              placement='bottomRight'
              visible={this.state.filter}
              onVisibleChange={this.onFilter.bind(this)}
            >
              <Button className={filterClasses}><img src={filterIcon}/></Button>
            </Popover> 

          </div>
          <ListHazards data={data}/>
        </div>
        <div className='container-right'>
          <div className='shadow'/>
          <MapComponent data={data}/> 
        </div>
        <div className='container-bottom'>
          <ChartPie data={staticdata}/>
          <ChartBar data={staticdata}/>
        </div>
      </div>
    );
  }
}

export default App
