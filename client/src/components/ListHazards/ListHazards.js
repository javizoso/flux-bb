import React from 'react';
import './ListHazards.scss';
import { observer, inject } from 'mobx-react';
import moment from 'moment'
import icon from '../../../static/filter.png'

export default inject('hazard')(observer(function ListHazards({hazard}) {
  let data = hazard.visible
  if (!data || !data.length) return null
  console.log('rendering list', data)
        // <div className='title'>Hazards</div>

      // <div className='header'>
      //   <div className='controls'>
      //     <img src={icon}/>
      //   </div>
      // </div>
  return (
    <div className="list-hazards">
      <div className='list'>
        {data.map((h) => {
          let c = `item severity-${h.severity}`
          if (h.open) c += ' open'
          let start = h.start ? moment(h.start).format('L') : false
          let end = h.start ? moment(h.start).format('L') : false
          if (start === end) end = false
            return (
              <div className={c} key={h.id} onClick={hazard.open.bind(hazard, h.id)}>
                <div className='content'>
                  <div className='row'>
                    <div className='description'>{h.description}</div>
                  </div>
                  <div className='row'>
                    <div className='category'>{h.category}</div>
                    {end &&
                      <div className='date'>{start} - {end}</div>
                    }
                    {!end &&
                      <div className='date'>{start}</div>
                    }
                  </div>
                  <div className='row'>
                    {h.open && h.imageUrl &&
                      <div className='image'><img src={'/images/' + h.imageUrl}/></div>
                    }
                  </div>
                </div>
                {h.severity < 3 &&
                  <div className='controls'>
                    <div className='close' onClick={hazard.close.bind(hazard, h.id)}>x</div>
                  </div>
                }
              </div>
            )
        })}
      </div>
    </div>
  );
}))

                // <div className='planned'>{hazard.planned}</div>
