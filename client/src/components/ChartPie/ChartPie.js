import React from 'react';
import './ChartPie.scss';
import G2 from 'g2'
import { observer } from 'mobx-react';

@observer
export default class ChartPie extends React.Component { // eslint-disable-line react/prefer-stateless-function

  generate(data) {
    let d = {}
    let severities = ['red', 'amber', 'green'];
    data.map((el) => {
      if (el.severity == null) el.severity = 'none'
      if (!d[el.severity]) d[el.severity] = {name: severities[parseInt(el.severity) - 1], count: 0, value: 0}
      d[el.severity].count++
    })
    Object.values(d).map((el) => {
      el.value = (el.count / data.length) * 100
    })
    // var data = [
    //   {name: 'A', value: 56.33 },
    //   {name: 'B', value: 24.03},
    //   {name: 'C', value: 10.38},
    //   {name: 'D',  value: 4.77},
    //   {name: 'E', value: 0.91},
    //   {name: 'F', value: 0.2}
    // ];
    let values = Object.values(d)
    var Stat = G2.Stat;
    var chart = new G2.Chart({
      container: this.chart,
      forceFit: true,
      // height: 450
      height : 260,
      plotCfg: {
        margin: [40, 80, 70, 80],
      },
    });
    chart.source(values);
    chart.coord('theta', {
      radius: 1,
      inner: 0.35
    });
    // chart.legend('name', {
    //   position: 'bottom',
    //   itemWrap: true,
    //   formatter: function(val) {
    //     for(var i = 0, len = data.length; i < len; i++) {
    //       var obj = data[i];
    //       if (obj.name === val) {
    //         return val + ': ' + obj.value + '%'; 
    //       }
    //     }
    //   }
    // });
    chart.legend(false)
    chart.tooltip({
      title: null,
      map: {value: function(v) {
        return Math.round(v)
      }}
    });
    chart.intervalStack()
      .position(Stat.summary.percent('value'))
      .color('name', ['#d32f2f', '#FFA000', '#388E3C'])
      .label('name*..percent',function(name, percent){
        percent = (percent * 100).toFixed(2) + '%';
        return percent;
      });
    chart.render();
    var geom = chart.getGeoms()[0];
    var items = geom.getData();
    geom.setSelected(items[1]);
  }

  generate2(data) {
    let d = {}
    data.map((el) => {
      if (el.severity == null) el.severity = 'none'
      if (!d[el.severity]) d[el.severity] = {severity: el.severity, count: 0, value: 0}
      d[el.severity].count++
    })
    Object.values(d).map((el) => {
      el.value = (el.count / data.length) * 100
    })
    // var data = [
    //   {name: 'A', value: 56.33 },
    //   {name: 'B', value: 24.03},
    //   {name: 'C', value: 10.38},
    //   {name: 'D',  value: 4.77},
    //   {name: 'E', value: 0.91},
    //   {name: 'F', value: 0.2}
    // ];
    let severities = ['red', 'amber', 'green'];
    var Stat = G2.Stat;
    var chart = new G2.Chart({
      container: this.chart,
      forceFit: true,
      height: 450
    });
    let values = Object.values(d)
    chart.source(values);
    chart.coord('theta', {
      radius: 1,
      inner: 0.35
    });
    // console.log('d-->', values)
    chart.legend(false)
    // chart.legend('severity', {
    //   position: 'bottom',
    //   itemWrap: true,
    //   formatter: function(val) {
    //     for(var i = 0, len = data.length; i < len; i++) {
    //       var obj = data[i];
    //       if (obj.severity === val) {
    //         return val + ': ' + obj.value + '%'; 
    //       }
    //     }
    //   }
    // });
    chart.tooltip({
      title: null,
      map: {
        value: 'value'
      }
    });
    chart.intervalStack()
      .position(Stat.summary.percent('value'))
      .color('severity')
      // .label('severity*..percent',function(severity, percent){
      //   percent = (percent * 100).toFixed(2) + '%';
      //   return percent;
      // });
    chart.render();
    var geom = chart.getGeoms()[0];
    var items = geom.getData();
    geom.setSelected(items[1]);
  }

  componentDidMount() {
    this.generate(this.props.data)
  }

  render() {
    return (
      <div className='chart-pie-container'>
        <div className='title'>Percentage hazards (by severity)</div>
        <div className='chart-pie' ref={(d) => this.chart = d} />
      </div>
    );
  }
}
