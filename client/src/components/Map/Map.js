import React from 'react';
import './Map.scss';
import { observer } from 'mobx-react';

@observer
export default class Map extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props)
    // this.state = { markers: []}
    this.markers = []
    this.markermap = {}
  }

  createMap() {
    this.styledMapType = new google.maps.StyledMapType([{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"lightness":"-76"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}])
    this.map = new google.maps.Map(this.map, {
      center: {lat: 51.494967, lng: -0.146706},
      zoom: 18
    });
  }

  removePins() {
    if (this.markerCluster) this.markerCluster.clearMarkers();
    this.markers.map((m) => m.setMap(null))
    this.markers = []
  }

  addPins() {
    this.removePins()
    let data = this.props.data.slice()
    let locations = data.map((d) => {
      return {lat: d.lat, lng: d.lon, id: d.id}
    })
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    this.markers = locations.map((location, i) => {
      let marker = new google.maps.Marker({
        position: location,
        label: labels[i % labels.length],
        mapTypeControlOptions: {
          mapTypeIds: ['styled_map', 'satellite']
        }
      });
      this.markermap[i] = location.id
      this.map.mapTypes.set('styled_map', this.styledMapType);
      this.map.setMapTypeId('styled_map');
      marker.addListener('click', (function(i, id) {
        console.log('i/id', i, id)
        this.map.setCenter(marker.getPosition());
      }).bind(this, i, location.id));
      return marker
    });
    this.markerCluster = new MarkerClusterer(this.map, this.markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
  }

  componentDidMount() {
    this.createMap()
    this.addPins()
    // let map = new google.maps.Map(document.getElementById('map'), {
    // var locations = [
    //   {lat: 51.494967, lng: -0.146706},
    //   {lat: 51.494867, lng: -0.146606},
    //   {lat: 51.494767, lng: -0.146706}
    // ]

  }

  componentDidUpdate(prevProps, prevState) {
    console.log('MAP>>did update')
    this.addPins()
  }

  componentWillReceiveProps(nextProps) {
    console.log('MAP>>did receive props')
    this.addPins()
  }

  render() {
    console.log('map render')
    return (<div id='map' className="map" ref={(a) => this.map = a}/>);
  }
}
