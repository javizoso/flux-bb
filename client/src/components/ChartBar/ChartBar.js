import React from 'react';
import './ChartBar.scss';
import G2 from 'g2'
import { observer } from 'mobx-react';

@observer
export default class ChartBar extends React.Component { // eslint-disable-line react/prefer-stateless-function

  generate(data) {
    let d = {}
    data.map((el) => {
      if (el.category == null) el.category = 'none'
      if (!d[el.category]) d[el.category] = {category: el.category, count: 0}
      d[el.category].count++
    })
    var chart = new G2.Chart({
      container: this.chart,
      width : 600,
      forceFit: true,
      // 'margin-bottom': 80,
      height : 240,
      plotCfg: {
        margin: [30, 140, 15, 60],
      },
    });
    let values = Object.values(d)
    chart.source(values, {category: {alias: ' '}, count: {alias: ' '}})
    chart.interval().position('category*count').color('category', ['#d32f2f', '#FFA000', '#FBC02D', '#AFB42B', '#388E3C'])
    chart.render(); 
  }

  componentDidMount() {
    this.generate(this.props.data)
  }

  render() {
    return (
      <div className='chart-bar-container'>
        <div className='title'>Total hazards (by category)</div>
        <div className='chart-bar' ref={(d) => this.chart = d} />
      </div>
    );
  }
}
