function handleResponse(resp) {
  const redirect = resp.headers.get('Location');
  if (redirect) {
    window.location.replace(redirect);
    return Promise.reject({ redirect });
  }

  const contentType = resp.headers && resp.headers.get('Content-Type');
  const isJSON = contentType && contentType.includes('json');
  const response = resp[isJSON ? 'json' : 'text']();
  return resp.ok ? response : response.then(err => { throw err; });
}

function getCookie(key) {
  const cookieValue = document.cookie.match(`(^|;)\\s*${key}\\s*=\\s*([^;]+)`);
  return cookieValue ? cookieValue.pop() : '';
}

export default function request(url, body, postForm = false) {
  const requestURL = `/${url.trimLeft('/')}`;
  const requestToken = getCookie('token');
  const requestOptions = { credentials: 'include', headers: {} };
  if (body && postForm) {
    const formData = new FormData();
    Object.keys(body).map((k) => formData.append(k, body[k]));
    requestOptions.method = 'POST';
    requestOptions.body = formData;
  } else if (body) {
    requestOptions.method = 'POST';
    requestOptions.body = JSON.stringify(body);
    requestOptions.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
  }

  // Append token to the headers
  requestOptions.headers.token = requestToken;
  return fetch(requestURL, requestOptions).then(handleResponse);
}
