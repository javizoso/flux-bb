import { observable, action } from 'mobx';
import request from '../lib/request'

const generateId = () => {
  return Math.round(Math.random() * 10000) + 200
}

console.log('request', request)
export default class Hazard {

  // @observable hazards = {};

  constructor() {
    request('api/hazards').then(this.loadHazards.bind(this))
    this.filters = { date: 0, red: true, amber: true, green: false, health: true, excavations: true, catastrophe: true, pv: true, height: true, driving: true}
    this.hazards = observable([])
    this.visible = observable([])
  }

  @action setFilter(type, e) {
    this.filters[type] = e.target.checked
    this.computeFilters()
  }

  @action computeFilters() {
    let now = new Date()
    let then = new Date(now.getTime() + 7 * 24 * 60 * 60 * 1000);
    let severity = ['red', 'amber', 'green']
    // let category = ['health', 'excavations', 'catastrophe', 'pv', 'height', 'driving']
    let category = ['pv', 'height', 'electricity', 'lifting', 'driving', 'buried', 'excavations', 'traffic', 'health', 'catastrophe']
    let categoryMap = {
      pv: "People Vehicle Plant Interface",
      height: "Working at Height",
      electricity: "Electricity",
      lifting: "Lifting",
      driving: "Driving",
      buried: "Buried Services",
      excavations: "Excavations",
      traffic: "Traffic Management",
      health: "Health",
      catastrophe: "Catastrophic Events"
    }
    let severityMap = {
      red: 1,
      amber: 2,
      green: 3
    }
    // let categoryMap = {
    //   'health': 'Health',
    //   'excavations': 'Excavations',
    //   'catastrophe': 'Catastrophe',
    //   'pv': 'People Vehicle Plant',
    //   'driving': 'Driving',
    //   'height': 'Working At Height'
    // }
    let l = this.visible.length
    for (var i = 0; i < l; i++) {
      this.visible.pop()
    }
    for (var i = 0; i < this.hazards.length; i++) {
      let visible = true
      let h = this.hazards[i]
      for (var j = 0; j < severity.length; j++) {
        let s = severity[j]
        if (h.severity === severityMap[s] && (!this.filters[s])) {
          visible = false
        }
      }
      for (var j = 0; j < category.length; j++) {
        let s = category[j]
        if (h.category === categoryMap[s] && (!this.filters[s])) {
          visible = false
        }
      }
      if (this.filters.date === 0) {
        if (new Date(h.start) > then) visible = false
      }
      if (visible) this.visible.push(h)
    }
      console.log('compute filters', this.visible)
  }

  @action setDate(v) {
    this.filters.date = v;
    this.computeFilters()
    this.sort()
  }

  @action sort() {
    this.visible.sort((a, b) => {
      if (a.severity > b.severity) return -1
      else if (a.severity < b.severity) return 1
      else {
        if (a.start > b.start) return 1
        else if (a.start < b.start) return -1
        return 0
      }
    })
  }

  @action loadHazards(data) {
    // console.log('load', data)

    // debugger
    // for (var i = 0; i < data.length; i++) {
    //   this.hazards.push(data[i])
    // }
    for (var i = 1; i < data.length; i++) {
      let hazard = {
        description: data[i][0],
        severity: data[i][1],
        lat: data[i][2],
        lon: data[i][3],
        category: data[i][4],
        start: data[i][5],
        end: data[i][6],
        planned: data[i][7],
        id: data[i][9] || generateId(),
        open: false,
        imageUrl: data[i][10] || false
      }
      this.hazards.push(hazard)
      // this.visible.push(hazard)
    }
    this.computeFilters()
    this.sort()
    // this.hazards.list = data
    // this.hazards = data
    // console.log('load hazards', this.hazards)
  }

  @action create(data) {
    console.log('create hazard', data)
    data.open = true
    data.id = generateId()
    this.hazards.push(data)
    this.visible.push(data)
  }

  @action filter(categories) {
    for (var i = 0; i < this.visible.length; i++) {
      this.visible.pop()
    }
    for (var i = 0; i < this.hazards.length; i++) {
      if (categories.indexOf(this.hazards[i].category) > -1) {
        this.visible.push(this.hazards[i])
      }
    }
  }

  @action open(id) {
    for (var i = 0; i < this.visible.length; i++) {
      if (this.visible[i].id == id) this.visible[i].open = true
      else this.visible[i].open = false
    }
  }

  @action close(id) {
    console.log('close', id)
    this.hazards.map((h, i) => {
      if (h.id === id) h.severity = 3
    })
    this.computeFilters()
    this.sort()
  }

  @action update(data) {
    // console.log('updating')
    let l = this.hazards.length
    for (var i = 0; i < l; i++) {
      this.hazards.pop()
    }
    l = this.visible.length
    for (var i = 0; i < l; i++) {
      this.visible.pop()
    }
    for (var i = 1; i < data.length; i++) {
      let hazard = {
        description: data[i][0],
        severity: data[i][1],
        lat: data[i][2],
        lon: data[i][3],
        category: data[i][4],
        start: data[i][5],
        end: data[i][6],
        planned: data[i][7],
        id: data[i][9] || generateId(),
        open: false,
        imageUrl: data[i][10] || false
      }
      this.hazards.push(hazard)
      // this.visible.push(hazard)
    }
    this.computeFilters()
    this.sort()
    // this.visible.sort((a, b) => {
    //   if (a.start > b.start) return 1
    //   else if (a.start < b.start) return -1
    //   return 0
    // })
  }

  @action updateHazard(data) {
    console.log('updating with', )
    request("api/hazards", data)
      .then((data) => {
        console.log('@!!', data)
      })
  }
}
