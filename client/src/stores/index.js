import request from '../lib/request';
import Common from './common';
import Hazard from './hazard';

const ws = new WebSocket(`ws${window.location.protocol === 'https:' ? 's' : ''}://${window.location.host}/ws`);
export const stores = () => {
  let hazard = new Hazard();
  ws.onmessage = (a) => {
    // console.log('>>', a)
    let data = JSON.parse(a.data)
    if (data.type === 'CREATE') {
      hazard.create(data.value)
    } else if (data.type === 'UPDATE') {
      hazard.update(data.value)
    } else if (data.type === '???') {
      hazard.dunno(data) 
    }
  }
  return { hazard }
};

export default stores(window.STATE || {});
