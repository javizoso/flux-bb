import { observable } from 'mobx';

export default class Common {

  @observable title = 'Flux Kit'
  @observable statusCode = 200
  @observable hostname = 'localhost'

  constructor(request, state = {}) {
    this.request = request;
    if (state.title != null) this.title = state.title;
  }

  setTitle(newTitle) {
    this.title = newTitle;
  }
}
