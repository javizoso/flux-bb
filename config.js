module.exports = {
  debug: true,
  port: 3001,
  url: 'http://localhost:3001',
  flux_url: 'https://flux.io',
  flux_client_id: '13ca3737-d8f7-47da-9ed0-709a5d4af101',
  flux_client_secret: 'bc22a6bb-f633-4744-b27e-9aa9ac959d63',
  session: {
    name: 'session',
    secret: '123456'
  },
  db: {
    database: 'bb',
    user: 'bb',
    password: '123456',
    options: {
      dialect: 'sqlite',
      sync: { force: true },
      // port: 5432,
      storage: 'database.sqlite',
      logging: false,
    }
  },
};
