module.exports = (sequelize, DataType) => {
  const hazard = sequelize.define('hazard', {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    description: {
      type: DataType.STRING,
      allowNull: false,
    },
    severity: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    lat: {
      type: DataType.FLOAT,
      allowNull: false,
    },
    lon: {
      type: DataType.FLOAT,
      allowNull: false,
    },
    category: {
      type: DataType.STRING,
      allowNull: true,
    },
    start: {
      type: DataType.DATE,
      allowNull: false,
    },
    end: {
      type: DataType.DATE,
      allowNull: true,
    },
    // level: {
    //   type: DataType.INTEGER,
    //   defaultValue: 0,
    //   allowNull: false,
    // },
    planned: {
      type: DataType.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },
    image: {
      type: DataType.STRING,
      allowNull: true,
    },
  });
  return hazard;
};
