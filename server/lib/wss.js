const WebSocket = require('ws');
const Logger = require('nice-logger');

function initWSS(app, sessionParser, options) {
  const log = options.log || new Logger('flux-blue:wss');
  const wss = new WebSocket.Server(options, function(e, a) {
    console.log(e, a);
  });
  wss.users = {};

  log.debug('running');

  wss.on('error', console.log.bind(console, 'WEBSOCKET_ERROR'));

  wss.on('connection', (ws) => {
    log.debug('Connection', ws);
    ws.on('message', function(msg) {
      wss.broadcast(msg);
    });
  });

  wss.broadcast = function(msg) {
    wss.clients.forEach(function(client) {
      if (client !== wss && client.readyState === WebSocket.OPEN) {
        client.send(msg);
      }
    });
  };

  app.wss = wss;
  return wss;
}

module.exports = initWSS;
