const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const c = require('../../config');

const sequelize = new Sequelize(c.db.database, c.db.user, c.db.password, c.db.options);
const db = { sequelize, Sequelize };
// const models = {};
const dir = path.join(__dirname, '..', 'models');

// fs.readdirSync(dir).forEach((file) => {
//   const modelDir = path.join(dir, file);
//   const model = sequelize.import(modelDir);
//   models[model.name] = model;
// });
// let hazard = require('../models/hazard')
let models = {
  hazard: sequelize.import('../models/hazard')
}

Object.keys(models).forEach((key) => {
  // if (typeof models[key].associate === 'function') {
  //   models[key].associate(models);
  // }
  models[key].sync();
});

Object.assign(db, models);

module.exports = db;
