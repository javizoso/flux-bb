const Translate = require('@google-cloud/translate');
const translate = Translate();

module.exports = function(text, target) {
  target = target || 'en';
  return translate.translate(text, target)
    .then((results) => {
      let translations = results[0];
      let translation = Array.isArray(translations) ? translations[0] : translations;
      return translation
    })
    .catch((err) => console.error('ERROR:', err))
}
