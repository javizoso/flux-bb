const fs = require('fs');
const path = require('path');
const https = require('https');
const http = require('http');
const crypto = require('crypto');
const url = require('url');
const FluxSdk = require('flux-sdk-node');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
// const logger = require('morgan');
const config = require('../config');
// let favicon = require('serve-favicon')
const sessionStore = new session.MemoryStore();
const cookies = cookieParser(config.session_secret);
const routes = require('./routes');
const credentials = require('./lib/credentials')

const app = express();
const initWSS = require('./lib/wss')
app.db = require('./lib/db');
const server = http.createServer(app);
const Logger = require('nice-logger');
const argv = require('minimist')(process.argv.slice(2));
const log = new Logger('flux-bb', argv.env === 'prod' ? 'error' : 'debug');
const projectId = 'aKjJnmebaRByZ7KWe'
const cellId = '40634930f3eabb73f2f6b516a5cb34d9';
app.plannedHazards = []
var cors = require('cors')



const sdk = new FluxSdk(config.flux_client_id, {
  clientSecret: config.flux_client_secret,
  fluxUrl: config.flux_url,
});

function fetchFlux() {
  let user = sdk.getUser(credentials)
  return user.getCell(projectId, cellId).fetch()
    .then((data) => {
      data.value.map((d, i) => d.push(i))
      app.plannedHazards = data.value
      return data.value
    })
}
fetchFlux()

let httpsOptions;
let httpsServer;
if (config.prod) {
  httpsOptions = {
    key: fs.readFileSync(config.ssl.key),
    cert: fs.readFileSync(config.ssl.cert),
    ca: fs.readFileSync(config.ssl.ca),
  };
  httpsServer = https.createServer(httpsOptions, app);
}



app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors())
// app.use(favicon(__dirname + '/../client/dist/img/favicon.ico'))
app.use(express.static('client/dist'));
app.use("/images", express.static(path.resolve(__dirname, './images/')));
app.use(cookies);

const sessionParser = session({
  secret: config.session.secret,
  name: config.session.name,
  resave: true,
  saveUninitialized: true,
  store: sessionStore,
});

app.use(sessionParser);

initWSS(app, sessionParser, {
  server,
  log,
  path: '/ws',
  clientTracking: true,
  perMessageDeflate: false,
});


// app.use((req, res, next) => {
//   req.credentials = req.session.fluxCredentials;
//   next();
// });

// var allowCrossDomain = function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', 'example.com');
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//     res.header('Access-Control-Allow-Headers', 'Content-Type');

//     next();
// }
// app.all('/', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "X-Requested-With");
//   next();
//  });

function generateRandomValue() {
  return crypto.randomBytes(24).toString('hex');
}

// function ensureSecure(req, res, next) {
//   if (req.secure || !config.prod) return next();
//   return res.redirect(`https://${req.get('host')}${req.url}`);
// }

// app.use(ensureSecure);
// app.use(allowCrossDomain);

app.get('/api/login', (req, res, next) => {
  const redirectUri = url.resolve(config.url, 'api/login');
  if (!req.query.code) {
    req.session.state = generateRandomValue();
    req.session.nonce = generateRandomValue();
    const authorizeUrl = sdk.getAuthorizeUrl(req.session.state, req.session.nonce, { redirectUri });
    return res.redirect(authorizeUrl);
  }
  return sdk.exchangeCredentials(req.session.state, req.session.nonce, req.query, { redirectUri })
    .then((credentials) => {
      console.log('creds', credentials)
      req.session.state = null;
      req.session.nonce = null;
      const t = credentials.idToken.payload;
      const user = {
        id: t.email,
        credentials: JSON.stringify(credentials),
      };
      req.session.user = user;
      req.session.credentials = credentials;
      res.cookie('doc', 'brown');
      // db.user.upsert(user).then((updated) => {
      //   return res.redirect('/');
      // }).catch(next);
    }).catch(next);
});

app.get('/logout', (req, res) => {
  req.session.destroy(() => res.redirect('/'));
});

// app.use('/api', (req, res, next) => {
//   if (req.session.credentials) {
//     req.user = sdk.getUser(req.session.credentials);
//   }
//   next();
// });

routes(app);

// app.get('/', (req, res) => {
//   res.sendFile('index.html', { root: 'client/dist' });
// });




createWebsocket(projectId)
var tables = {}

function getDataTable(pid, credentials) {
  let dt = new sdk.Project(credentials, pid).getDataTable()
  let table = { table: dt, handlers: {}, websocketOpen: false }
  return table
}

function createWebsocket(pid) {
  let dt = getDataTable(projectId, credentials)
  const websocketRefHandler = (msg) => {
    for (var k in dt.handlers) {
      dt.handlers[k](msg)
    }
  }
  let userId = credentials.idToken.payload.email
  dt.handlers[userId] = handleChange.bind(null, pid)
  if (!dt.websocketOpen) {
    dt.websocketOpen = true
    var options = {
      // onError: checkWebsocket.bind(null, pid, user)
    }
    let ws = dt.table.openWebSocket(options)
    dt.table.addWebSocketHandler(websocketRefHandler)
  }
}

function handleChange(projectId, msg) {
  // console.log('handleCHange', msg.type)
  if (msg.type === 'CELL_DELETED') {
    // let keys = `["${msg.body.id}"]`
    // db.schedule.destroy({where: {keyId: msg.body.id}})
    // return db.schedule.destroy({where: {keys: keys}})
  } else if (msg.type === 'CELL_MODIFIED') {
    // if (msg.body.clientName === 'Flux Scheduler') return
    // let keyId = msg.body.id
    // console.log('>>changed')
    fetchFlux()
      .then((data) => {
        // console.log('ws send data', data)
        app.wss.broadcast(JSON.stringify({type: 'UPDATE', value: data}))
      })
    // db.schedule.findAll({where: {projectId: projectId}})
    // console.log('cell modified')
  }
}































// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', { message: err.message, error: {} });
});

// server.listen(80);
server.listen(config.port);
if (config.prod) httpsServer.listen(config.ssl.port);
