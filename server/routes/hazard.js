const multer = require('multer');
// const upload = multer();
const upload = multer({storage: multer.MemoryStorage})
const translate = require('../lib/translate')

const path = require('path');
const staticPath = path.resolve(__dirname, '../images/');
const fs = require('fs');
const parser = require('exif-parser')
const twilio = require('twilio');
const accountSid = 'AC74e6c6a2572cbe395697dfb834996260';
const authToken = 'c699fb6532a6daa80adf069bc8de5283';
const twilioClient = new twilio.RestClient(accountSid, authToken);

// let twilioNumber = '07484877559'
// let numbers = ['07878641721']
// let twilioNumber = '+447484877559'
let twilioNumber = '+442870032032'
let amberNumbers = ['+447878641721', '+447484877559']
let redNumbers = ['+447878641721', '+447515396250', '+447484877559']

// ---- GCloud storage -----

const CLOUD_KEY = '/gcs-admin-key.json'
const CLOUD_BUCKET = 'app-hazard'
const CLOUD_PROJECT = 'concise-slate-165007'
const Storage = require('@google-cloud/storage');
const storage = Storage({projectId: CLOUD_PROJECT, keyFilename: CLOUD_KEY});
const bucket = storage.bucket(CLOUD_BUCKET);

bucket.acl.default.add({
  scope: "allUsers",
  role: Storage.acl.READER_ROLE
}, function (err) {})

function getPublicUrl (filename) {
  return `https://storage.googleapis.com/${CLOUD_BUCKET}/${filename}`;
}

function sendUploadToGCS (req, res, next) {
  if (!req.file) return next();
  const gcsname = Date.now() + req.file.originalname;
  const file = bucket.file(gcsname);

  const stream = file.createWriteStream({
    metadata: { 
      contentType: req.file.mimetype,
      cacheControl: "public, max-age=300"
    }
  });

  stream.on('error', (err) => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on('finish', () => {
    req.file.cloudStorageObject = gcsname;
    req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
    next();
  });

  stream.end(req.file.buffer);
}

// ---------------


// ---- GCLOUD SPEECH -----
// var speech = require('@google-cloud/speech');


module.exports = (app) => {
  // List hazards
  app.get('/api/hazards', (req, res) => {
    // console.log('get', app.plannedHazards.length)
    // res.json(app.plannedHazards)
    app.db.hazard.findAll()
      .then((result) => {
        let r = result.map((record) => {
          let json= record.toJSON()
          json.imageUrl = json.image
          // return json
          return [
            json.description, 
            json.severity, 
            json.lat,
            json.lon,
            json.category,
            json.start,
            json.end,
            0,
            0,
            json.id + 10000,
            json.image
          ]
        })
        // console.log('res', res)
        // console.log('existing', app.plannedHazards[0])
        let all = app.plannedHazards.slice(0).concat(r)
        console.log('all', all.slice(0, 10))
        res.json(all)
        // res.json(result)
      });
  });

  // Create hazard
  app.post('/api/hazards', upload.single('image'), (req, res) => {
    let extension = req.file.originalname.split('.')[1]
    let filename = Math.round(Math.random() * 1000) + '.' + extension
    let path = `${staticPath}/${filename}`
    // console.log('got post', req.body, req.file, path, filename)
    let data = req.body
    data.severity = parseInt(data.severity)
    // translate(data.description)
    //   .then((translated) => {
      // ---- TWILIO ----
        // data.description = translated
        if (data.severity === 1) {
          let body = `Red Alert: ${data.description}. Please check the dashboard: https://mobile.b4bt.co.uk`
          redNumbers.map((n) => {
            twilioClient.messages.create({
              body: body,
              to: n,
              from: twilioNumber
            }, function(err, message) {
              console.log('cb', err, message);
            });
          })
          // console.log('should send', body)
        } else if (data.severity === 2) {
          let body = `Amber Alert: ${data.description}. Please check the dashboard: https://mobile.b4bt.co.uk`
          redNumbers.map((n) => {
            twilioClient.messages.create({
              body: body,
              to: n,
              from: twilioNumber
            }, function(err, message) {
              console.log('cb', err, message);
            });
          })
        }
      // ----------------

      fs.writeFile(path, req.file.buffer, function(err) {
        // let parsed = parser.create(req.file.buffer).parse()
        // console.log('parsed', parsed)
        // console.log('path', path, err)
        if (err) throw err;
        // data.value = path
        data.imageUrl = filename
        data.image = filename
        data.start = new Date()
        data.category = 'None'
        data.lat = 51.494809
        data.lon = -0.146964 
        delete data.level
        let msg = JSON.stringify({type: 'CREATE', value: data})
        app.db.hazard.create(data)
          .then((result) => 
            res.json(data)
          )
          .catch((error) => res.status(412).json({ msg: error.message }));

        app.wss.broadcast(msg);
        // res.end(msg);
      });
    // })
  });

  // ---- GCloud storage -----
  // app.post('/api/hazards', upload.single('image'), sendUploadToGCS, (req, res) => {
  //   let data = req.body
  //   if (req.file && req.file.cloudStoragePublicUrl) {
  //     data.imageUrl = req.file.cloudStoragePublicUrl;
  //   }
  //   // ---- TWILIO ----
  //   if (data.severity === 1) {
  //     let body = `Hazard Alert: ${data.description}. Please check the dashboard: http://mobile.b4bt.co.uk`
  //     // twilioClient.messages.create({
  //     //   body: body,
  //     //   to: '+12345678901',
  //     //   from: '+12345678901'
  //     // }, function(err, message) {
  //     //   console.log(message.sid);
  //     // });
  //     console.log('should send', body)
  //   }
  //   // ----------------
  //   let msg = JSON.stringify(data)
  //   app.db.hazard.create(data)
  //     .then((result) => res.json(data))
  //     .catch((error) => res.status(412).json({ msg: error.message }));
  //   app.wss.broadcast(msg);
  // })
  // --------------------


  // Get hazard
  app.get('/api/hazards/:id', (req, res) => {
    app.db.hazard.findOne({ where: { id: req.params.id } })
      .then((result) => {
        if (result) res.json(result);
        else res.sendStatus(404);
      })
      .catch((error) => res.status(412).json({ msg: error.message }));
  });

  // Update hazard
  app.put('/api/hazards/:id', (req, res) => {
    app.db.hazard.update(req.body, { where: { id: req.params.id } })
      .then(() => res.sendStatus(204))
      .catch((error) => res.status(412).json({ msg: error.message }));
  });

  // Delete hazard
  app.delete('/api/hazards/:id', (req, res) => {
    app.db.hazard.destroy({ where: { id: req.params.id } })
      .then(() => res.sendStatus(204))
      .catch((error) => res.status(412).json({ msg: error.message }));
  });
};
